  int ledrp1 = 2;//on stocke dans une variable de type entier (int) l'adreese des sortie branchée.
  int ledvp1 = 3;//ici on stocke dans la variable ledvp1 (pour led verte pieton 1) l'entier 3 (qui correspond à la borne reliée sur le montage.
  int ledv1 = 4;
  int ledj1 = 5;
  int ledr1 = 6;
  int ledrp2 = 7;
  int ledvp2 = 8;
  int ledv2 = 9;
  int ledj2 = 10;
  int ledr2 = 11;
  int nb =48;
  int temp=48;



void setup() {//debut de la boucle d'iitialisation qui s'execute une seule fois au démarrage.



  Serial.begin(9600);//on démare la connection série
  Serial.print("setup");//on ecrit sur le port série "setup" pour indiquer que l'on commence
  pinMode(ledrp1, OUTPUT);//on indique que le pin (ou la borne) numéro "ledrp1" (la borne du numéro que l'on a stocké dans la variable) et de type sortie (output, cad que l'on pilote la tension)
  pinMode(ledvp1, OUTPUT);
  pinMode(ledv1, OUTPUT);
  pinMode(ledj1, OUTPUT);
  pinMode(ledr1, OUTPUT);
  pinMode(ledrp2, OUTPUT);
  pinMode(ledvp2, OUTPUT);
  pinMode(ledv2, OUTPUT);
  pinMode(ledj2, OUTPUT);
  pinMode(ledr2, OUTPUT);



  digitalWrite(ledv1, HIGH);//on indique que le pin (ou la borne) numéro "ledrp1" (la borne du numéro que l'on a stocké dans la variable) et de type sortie (output, cad que l'on pilote la tension)
  digitalWrite(ledv2, HIGH);
  delay(500);
  
  digitalWrite(ledj1, HIGH);
  digitalWrite(ledj2, HIGH);
  delay(500);// on attend une demi seconde

  digitalWrite(ledr1, HIGH);
  digitalWrite(ledr2, HIGH);
  delay(500);
  
  digitalWrite(ledvp1, HIGH);
  digitalWrite(ledvp2, HIGH);
   delay(500);
    
  digitalWrite(ledrp1, HIGH);
  digitalWrite(ledrp2, HIGH);
  delay(500);
  for (int i=0; i<5;i++){// boucle for, on commence à i=0 et on fait +1 jusqu'à i=5.
    
    digitalWrite(ledv1, LOW);// cette fois on fixe la sortie ledv1 à sa valeur basse cad 0V
    digitalWrite(ledv2, LOW);
    digitalWrite(ledj1, LOW);
    digitalWrite(ledj2, LOW);
    digitalWrite(ledr1, LOW);
    digitalWrite(ledr2, LOW);
    digitalWrite(ledvp1, LOW);
    digitalWrite(ledvp2, LOW);
    digitalWrite(ledrp1, LOW);
    digitalWrite(ledrp2, LOW);

    delay(100);// on attend 100 millisecondes

    digitalWrite(ledv1, HIGH);// on fixe les sorties à 5V...
    digitalWrite(ledv2, HIGH);
    digitalWrite(ledj1, HIGH);
    digitalWrite(ledj2, HIGH);
    digitalWrite(ledr1, HIGH);
    digitalWrite(ledr2, HIGH);
    digitalWrite(ledvp1, HIGH);
    digitalWrite(ledvp2, HIGH);
    digitalWrite(ledrp1, HIGH);
    digitalWrite(ledrp2, HIGH);

  }
  Serial.print("loop");// on écrit sur le port série "loop" (le port série c'est la connection USB, et on peut lire c'est commentaire avec le moniteur série
  delay(2000);

}

void loop() {
  
  if(nb<=48){
    passage1();
  }
  for(int i =0;i<100;i++){
    if (Serial.available() > 1) {//on regarde si il y a un truc à lire sur le port série
    
      temp=Serial.read();
      if (temp==49 or temp==48){
        nb=temp;//on lit et stocke ce qui est envyé par le monietur
      }
      
    }
    
    if(nb>48){// en fonction du résultat on est soit en incident soit en mode normal
      incident();
    }
    delay(100);
  }
  if(nb<=48){
    passage2();
  }
  for(int i =0;i<100;i++){#permet de vider la memoire tampon (c'est moche on peut faire mieux;) )
    if (Serial.available() > 1) {
    
      temp=Serial.read();
      if (temp==48 or temp==49){
        nb=temp;
      }
      //Serial.print("yes");
      //Serial.print(nb);
      
    }
    
    if(nb>48){
      incident();
    }
    delay(100);
  }
}

void passage1(){

  Serial.print("Passage 1");// on écrit sur le port série que l'on passe la voie 1 au vert
  delay(50);// on attend 50 ms
  digitalWrite(ledv2, LOW);//on etteind les feux verts de la voie 2
  digitalWrite(ledj2, HIGH);// on allume les feux orange de la voie2
  digitalWrite(ledrp1, HIGH);// on passe les feux pieton au rouge
  digitalWrite(ledrp2, HIGH);
  digitalWrite(ledvp1, LOW);// on etteind les feux vert des pietons.
  digitalWrite(ledvp2, LOW);
  //Serial.print("feu 2 orange");
  delay(2000);// on attend 2s avant de passer au rouge.
  digitalWrite(ledj2, LOW);//on ettiend le feux orange
  digitalWrite(ledr2, HIGH);// on allume le rouge voie 2
  digitalWrite(ledv1, HIGH);// on passe ou vert en voie 1
  digitalWrite(ledr1, LOW);//on netteind le rouge de la voie 1 (car on est ou vert)
  digitalWrite(ledj1, LOW);// on etteind le jaune aussi (au cas où)
  digitalWrite(ledvp2, HIGH);//on passe les pietons voie 2 au vert
  digitalWrite(ledrp2, LOW);// on etteind le feux rouge pieton.
  //Serial.print("feu 1 vert");
}//fin de la fonction
void passage2(){

  Serial.print("Passage 2");//meme code que passage 1 saufe que l'on remplace les 1 par 2 et 2 par 1.

  digitalWrite(ledv1, LOW);
  digitalWrite(ledj1, HIGH);
  digitalWrite(ledrp2, HIGH);
  digitalWrite(ledrp1, HIGH);
  digitalWrite(ledvp2, LOW);
  digitalWrite(ledvp1, LOW);
  //Serial.print("feu 1 orange");
  delay(2000);
  digitalWrite(ledj1, LOW);
  digitalWrite(ledr1, HIGH);
  digitalWrite(ledv2, HIGH);
  digitalWrite(ledr2, LOW);
  digitalWrite(ledj2, LOW);
  digitalWrite(ledvp1, HIGH);
  digitalWrite(ledrp1, LOW);
  //Serial.print("feu 2 vert");
}

void incident(){//pilotage en mode incident des feux
    digitalWrite(ledv1, LOW);
    digitalWrite(ledv2, LOW);
    digitalWrite(ledj1, LOW);
    digitalWrite(ledj2, LOW);
    digitalWrite(ledr1, LOW);
    digitalWrite(ledr2, LOW);
    digitalWrite(ledvp1, LOW);
    digitalWrite(ledvp2, LOW);
    digitalWrite(ledrp1, LOW);
    digitalWrite(ledrp2, LOW);
    delay(1000);
    digitalWrite(ledj1, HIGH);
    digitalWrite(ledj2, HIGH);
    delay(1000);
}
