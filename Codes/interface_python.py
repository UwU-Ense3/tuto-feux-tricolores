from tkinter import *#importation des module
import serial
from time import sleep

def echap():#fonction qui permet de sortir de la fenetre tkinter
    fen.destroy()

def passage1():#change les couleurs des feux et fleches pour le passage de la voie 1
    dessin.itemconfig(fleche1,fill="grey")
    dessin.itemconfig(fleche2,fill="grey")
    dessin.itemconfig(fleche1bis,fill="grey")

    dessin.itemconfig(vert1,fill="black")
    dessin.itemconfig(vert2,fill="black")
    dessin.itemconfig(vert3,fill="black")
    dessin.itemconfig(vert4,fill="black")
    dessin.itemconfig(jaune1,fill="black")
    dessin.itemconfig(jaune2,fill="yellow")
    dessin.itemconfig(jaune3,fill="black")
    dessin.itemconfig(jaune4,fill="yellow")
    dessin.itemconfig(rouge1,fill="red")
    dessin.itemconfig(rouge2,fill="black")
    dessin.itemconfig(rouge3,fill="red")
    dessin.itemconfig(rouge4,fill="black")

    dessin.update()#actualise l'affichage
    sleep(2)#attend
    dessin.itemconfig(fleche1,fill="green")#passe les feux au vert
    dessin.itemconfig(fleche1bis,fill="green")

    dessin.itemconfig(vert1,fill="green")
    dessin.itemconfig(vert3,fill="green")
    dessin.itemconfig(jaune2,fill="black")
    dessin.itemconfig(jaune4,fill="black")
    dessin.itemconfig(rouge1,fill="black")
    dessin.itemconfig(rouge2,fill="red")
    dessin.itemconfig(rouge3,fill="black")
    dessin.itemconfig(rouge4,fill="red")

    dessin.update()#actualise

def passage2():#meme que pour passage 1 mais avec la voie 2
    dessin.itemconfig(fleche1,fill="grey")
    dessin.itemconfig(fleche2,fill="grey")
    dessin.itemconfig(fleche1bis,fill="grey")

    dessin.itemconfig(vert1,fill="black")
    dessin.itemconfig(vert2,fill="black")
    dessin.itemconfig(vert3,fill="black")
    dessin.itemconfig(vert4,fill="black")
    dessin.itemconfig(jaune2,fill="black")
    dessin.itemconfig(jaune1,fill="yellow")
    dessin.itemconfig(jaune4,fill="black")
    dessin.itemconfig(jaune3,fill="yellow")
    dessin.itemconfig(rouge2,fill="red")
    dessin.itemconfig(rouge1,fill="black")
    dessin.itemconfig(rouge4,fill="red")
    dessin.itemconfig(rouge3,fill="black")

    dessin.update()
    sleep(2)
    dessin.itemconfig(fleche2,fill="green")
    dessin.itemconfig(fleche1bis,fill="green")

    dessin.itemconfig(vert2,fill="green")
    dessin.itemconfig(vert4,fill="green")
    dessin.itemconfig(jaune1,fill="black")
    dessin.itemconfig(jaune3,fill="black")
    dessin.itemconfig(rouge2,fill="black")
    dessin.itemconfig(rouge1,fill="red")
    dessin.itemconfig(rouge4,fill="black")
    dessin.itemconfig(rouge3,fill="red")

    dessin.update()

def anomalie():#fonction qui déclare le passage en mode incident
    global etat#change la valeur de la variable globale, c'est pas bien d'utiliser les variable globale, essayez de modifier le code pour vous en passer
    etat=1
    B2.configure(text='marche',command=normale)#change le texte et la fonction du bouton

def incident():#change l'affichage en mode incident

    ser.write('1'.encode('ascii'))#on écrit sur le port série pour passer l'arduino en mode incident
    dessin.itemconfig(fleche1,fill="grey")#change les couleurs des fleches et feux
    dessin.itemconfig(fleche1bis,fill="grey")
    dessin.itemconfig(fleche2,fill="grey")
    dessin.itemconfig(vert1,fill="black")
    dessin.itemconfig(vert2,fill="black")
    dessin.itemconfig(vert3,fill="black")
    dessin.itemconfig(vert4,fill="black")
    dessin.itemconfig(rouge1,fill="black")
    dessin.itemconfig(rouge2,fill="black")
    dessin.itemconfig(rouge3,fill="black")
    dessin.itemconfig(rouge4,fill="black")
    dessin.itemconfig(jaune1,fill="yellow")
    dessin.itemconfig(jaune2,fill="yellow")
    dessin.itemconfig(jaune3,fill="yellow")
    dessin.itemconfig(jaune4,fill="yellow")

    dessin.update()#actualise le dessin
    sleep(0.5)
    dessin.itemconfig(jaune1,fill="black")#attend et fait clignoter les feux
    dessin.itemconfig(jaune2,fill="black")
    dessin.itemconfig(jaune3,fill="black")
    dessin.itemconfig(jaune4,fill="black")


    dessin.update()
    sleep(0.5)

def normale():#permet de passer en mode normale
    B2.configure(text='incident',command=anomalie)#change le texte du bouton
    global etat#change la variable globale
    etat=0

def marche():#fonction boucle infinie
    if etat==1:#regarde l'etat
        incident()#affiche le mode incident (et dans la fonction utilise le port série pour faire passer l'arduino en mode incident
    else :#mode normal
        ser.write('0'.encode('ascii'))#dit à l'arduino de passer en mode normal
    donnee=str(ser.readline())#lit le port série
    #print(donnee[2:-1],etat)#permet d'afficher ou non ce qui est lu ssur le port série

    if donnee[2:-1]=="Passage 1" and etat==0:#si on a passage 1 et qu'on est en mode normal
        passage1()#alors on affiche le feux vert pour la voie 1

    elif donnee[2:-1]=="Passage 2" and etat==0:#idem pour la voie 2
        passage2()

    dessin.after(1,marche)#dessin boucle infinie





etat=0#declare la variable globale
fen=Tk()
fen.title("interface")
c=1000#echel du dessin depend de l'ecran à modifier pour avoir une fenetre plus grosse ou plus petite
dessin=Canvas(fen,height=c,width=c)#creer une feuille de dessin
dessin.pack()
#dessine la route les feux etc là j'ai la flem de tout commenter

route=dessin.create_polygon(c/3,0,2*c/3,0,2*c/3,c/3,c,c/3,c,2*c/3,2*c/3,2*c/3,2*c/3,c,c/3,c,c/3,2*c/3,0,2*c/3,0,c/3,c/3,c/3,fill="grey")

ligne4=dessin.create_polygon(c/3+c*0.02,c/3-c*0.02,c/3+c*0.02,c/3,c/2,c/3,c/2,c/6,c/2-c*0.01,c/6,c/2-c*0.01,c/3-c*0.02,fill="white")

ligne3=dessin.create_polygon(2*c/3,c/3+c*0.02,2*c/3,c/2,5*c/6,c/2,5*c/6,c/2-c*0.01,2*c/3+c*0.02,c/2-c*0.01,2*c/3+c*0.02,c/3+c*0.02,fill="white")

ligne2=dessin.create_polygon(2*c/3-c*0.02,2*c/3,2*c/3-c*0.02,2*c/3+c*0.02,c/2+c*0.01,2*c/3+c*0.02,c/2+c*0.01,5*c/6,c/2,5*c/6,c/2,2*c/3,fill="white")

ligne1=dessin.create_polygon(c/3,c/2,c/3,2*c/3-c*0.02,c/3-c*0.02,2*c/3-c*0.02,c/3-c*0.02,c/2+c*0.01,c/6,c/2+c*0.01,c/6,c/2,fill="white")

feu4=dessin.create_polygon(c/3-c/20,c/3,c/3-c/20,c/3-c/12,c/3-c/20+c*0.01,c/3-c/12,c/3-c/20+c*0.01,c/3-c/12-c/12,c/3-c/20-c*0.02,c/3-c/12-c/12,c/3-c/20-c*0.02,c/3-c/12,c/3-c/20-c*0.01,c/3-c/12,c/3-c/20-c*0.01,c/3,fill="black")

feu1=dessin.create_polygon(c/3-c/20,2*c/3+c/10,c/3-c/20,2*c/3+c/10-c/12,c/3-c/20+c*0.01,2*c/3+c/10-c/12,c/3-c/20+c*0.01,2*c/3+c/10-c/12-c/12,c/3-c/20-c*0.02,2*c/3+c/10-c/12-c/12,c/3-c/20-c*0.02,2*c/3+c/10-c/12,c/3-c/20-c*0.01,2*c/3+c/10-c/12,c/3-c/20-c*0.01,2*c/3+c/10,fill="black")

feu2=dessin.create_polygon(2*c/3+c/20,2*c/3+c/10,2*c/3+c/20,2*c/3+c/10-c/12,2*c/3+c/20+c*0.01,2*c/3+c/10-c/12,2*c/3+c/20+c*0.01,2*c/3+c/10-c/12-c/12,2*c/3+c/20-c*0.02,2*c/3+c/10-c/12-c/12,2*c/3+c/20-c*0.02,2*c/3+c/10-c/12,2*c/3+c/20-c*0.01,2*c/3+c/10-c/12,2*c/3+c/20-c*0.01,2*c/3+c/10,fill="black")

feu3=dessin.create_polygon(2*c/3+c/20,c/3,2*c/3+c/20,c/3-c/12,2*c/3+c/20+c*0.01,c/3-c/12,2*c/3+c/20+c*0.01,c/3-c/12-c/12,2*c/3+c/20-c*0.02,c/3-c/12-c/12,2*c/3+c/20-c*0.02,c/3-c/12,2*c/3+c/20-c*0.01,c/3-c/12,2*c/3+c/20-c*0.01,c/3,fill="black")

vert1=dessin.create_oval(c/3-c/20+c*0.01-c*0.03+c/200,2*c/3+c/10-c/12-c/36+c/200,c/3-c/20+c*0.01-c/200,2*c/3+c/10-c/12-c/200,fill="green",outline="green")

jaune1=dessin.create_oval(c/3-c/20+c*0.01-c*0.03+c/200,2*c/3+c/10-c/12-2*c/36+c/200,c/3-c/20+c*0.01-c/200,2*c/3+c/10-c/12-c/200-c/36,fill="Yellow",outline="yellow")

rouge1=dessin.create_oval(c/3-c/20+c*0.01-c*0.03+c/200,2*c/3+c/10-c/12-3*c/36+c/200,c/3-c/20+c*0.01-c/200,2*c/3+c/10-c/12-c/200-2*c/36,fill="red",outline="red")



vert2=dessin.create_oval(2*c/3+c/20+c*0.01-c*0.03+c/200,2*c/3+c/10-c/12-c/36+c/200,2*c/3+c/20+c*0.01-c/200,2*c/3+c/10-c/12-c/200,fill="green",outline="green")

jaune2=dessin.create_oval(2*c/3+c/20+c*0.01-c*0.03+c/200,2*c/3+c/10-c/12-2*c/36+c/200,2*c/3+c/20+c*0.01-c/200,2*c/3+c/10-c/12-c/200-c/36,fill="Yellow",outline="yellow")

rouge2=dessin.create_oval(2*c/3+c/20+c*0.01-c*0.03+c/200,2*c/3+c/10-c/12-3*c/36+c/200,2*c/3+c/20+c*0.01-c/200,2*c/3+c/10-c/12-c/200-2*c/36,fill="red",outline="red")



vert3=dessin.create_oval(2*c/3+c/20+c*0.01-c*0.03+c/200,c/3-c/12-c/36+c/200,2*c/3+c/20+c*0.01-c/200,c/3-c/12-c/200,fill="green",outline="green")

jaune3=dessin.create_oval(2*c/3+c/20+c*0.01-c*0.03+c/200,c/3-c/12-2*c/36+c/200,2*c/3+c/20+c*0.01-c/200,c/3-c/12-c/200-c/36,fill="Yellow",outline="yellow")

rouge3=dessin.create_oval(2*c/3+c/20+c*0.01-c*0.03+c/200,c/3-c/12-3*c/36+c/200,2*c/3+c/20+c*0.01-c/200,c/3-c/12-c/200-2*c/36,fill="red",outline="red")



vert4=dessin.create_oval(c/3-c/20+c*0.01-c*0.03+c/200,c/3-c/12-c/36+c/200,c/3-c/20+c*0.01-c/200,c/3-c/12-c/200,fill="green",outline="green")

jaune4=dessin.create_oval(c/3-c/20+c*0.01-c*0.03+c/200,c/3-c/12-2*c/36+c/200,c/3-c/20+c*0.01-c/200,c/3-c/12-c/200-c/36,fill="Yellow",outline="yellow")

rouge4=dessin.create_oval(c/3-c/20+c*0.01-c*0.03+c/200,c/3-c/12-3*c/36+c/200,c/3-c/20+c*0.01-c/200,c/3-c/12-c/200-2*c/36,fill="red",outline="red")

fleche1=dessin.create_polygon(c/3,c/2,c/3+c*0.03,c/2-c*0.02,c/3+c*0.03,c/2-c*0.01,2*c/3-c*0.03,c/2-c*0.01,2*c/3-c*0.03,c/2-c*0.02,2*c/3,c/2,2*c/3-c*0.03,c/2+c*0.02,2*c/3-c*0.03,c/2+c*0.01,c/3+c*0.03,c/2+c*0.01,c/3+c*0.03,c/2+c*0.02,fill="green")

fleche2=dessin.create_polygon(c/2,c/3,c/2+c*0.02,c/3+c*0.03,c/2+c*0.01,c/3+c*0.03,c/2+c*0.01,2*c/3-c*0.03,c/2+c*0.01,2*c/3-c*0.03,c/2+c*0.02,2*c/3-c*0.03,c/2,2*c/3,c/2-c*0.02,2*c/3-c*0.03,c/2-c*0.01,2*c/3-c*0.03,c/2-c*0.01,c/3+c*0.03,c/2-c*0.02,c/3+c*0.03,fill="green")

fleche1bis=dessin.create_polygon(c/2-c*0.01,c/2-c*0.01,c/2+c*0.01,c/2-c*0.01,c/2+c*0.01,c/2+c*0.01,c/2-c*0.01,c/2+c*0.01,fill="green")

B1=Button(fen, text='Echap',anchor=CENTER,bg='lightgrey',command=echap)#creer le bouton echap asocié à la fonction echap
B1.place(x=c*0.9,y=c*0.01)

B2=Button(fen, text='anomalie',anchor=CENTER,bg='lightgrey',command=anomalie)
B2.place(x=c*0.9,y=c*0.01+50)#creer un bouton asocié à la commande anomalie


ser = serial.Serial(port="COM3", baudrate=9600, timeout=1, writeTimeout=1)#ouvre la communication avec l'arduino
dessin.update()
marche()#lance la boucle infinie
fen.mainloop()


'''
ser = serial.Serial(port="COM3", baudrate=9600, timeout=1, writeTimeout=1)
print (ser)
while 1:
    donnee=str(ser.readline())
    print("donné:",donnee[2:-1])'''











