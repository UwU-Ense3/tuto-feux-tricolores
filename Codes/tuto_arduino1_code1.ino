  int ledrp1 = 2;//on stocke dans une variable de type entier (int) l'adreese des sortie branchée.
  int ledvp1 = 3;//ici on stocke dans la variable ledvp1 (pour led verte pieton 1) l'entier 3 (qui correspond à la borne reliée sur le montage.
  int ledv1 = 4;
  int ledj1 = 5;
  int ledr1 = 6;
  int ledrp2 = 7;
  int ledvp2 = 8;
  int ledv2 = 9;
  int ledj2 = 10;
  int ledr2 = 11;
  int nb =0;



void setup() {//debut de la boucle d'iitialisation qui s'execute une seule fois au démarrage.



  Serial.begin(9600);//on démare la connection série
  Serial.print("setup");//on ecrit sur le port série "setup" pour indiquer que l'on commence
  pinMode(ledrp1, OUTPUT);//on indique que le pin (ou la borne) numéro "ledrp1" (la borne du numéro que l'on a stocké dans la variable) et de type sortie (output, cad que l'on pilote la tension)
  pinMode(ledvp1, OUTPUT);
  pinMode(ledv1, OUTPUT);
  pinMode(ledj1, OUTPUT);
  pinMode(ledr1, OUTPUT);
  pinMode(ledrp2, OUTPUT);
  pinMode(ledvp2, OUTPUT);
  pinMode(ledv2, OUTPUT);
  pinMode(ledj2, OUTPUT);
  pinMode(ledr2, OUTPUT);



  digitalWrite(ledv1, HIGH);//digitalwrite pour indiquer que l'on pilote la sortie ledv1 en mode tout ou rien, et High pour dire que l'on la fixe à sa tension haute cad 5V.
  digitalWrite(ledv2, HIGH);
  delay(500);// on attend une demi seconde
  
  digitalWrite(ledj1, HIGH);//etc
  digitalWrite(ledj2, HIGH);
  delay(500);

  digitalWrite(ledr1, HIGH);
  digitalWrite(ledr2, HIGH);
  delay(500);
  
  digitalWrite(ledvp1, HIGH);
  digitalWrite(ledvp2, HIGH);
   delay(500);
    
  digitalWrite(ledrp1, HIGH);
  digitalWrite(ledrp2, HIGH);
  delay(500);
  for (int i=0; i<5;i++){// boucle for, on commence à i=0 et on fait +1 jusqu'à i=5.
    
    digitalWrite(ledv1, LOW);// cette fois on fixe la sortie ledv1 à sa valeur basse cad 0V
    digitalWrite(ledv2, LOW);
    digitalWrite(ledj1, LOW);
    digitalWrite(ledj2, LOW);
    digitalWrite(ledr1, LOW);
    digitalWrite(ledr2, LOW);
    digitalWrite(ledvp1, LOW);
    digitalWrite(ledvp2, LOW);
    digitalWrite(ledrp1, LOW);
    digitalWrite(ledrp2, LOW);

    delay(100);// on attend 100 millisecondes

    digitalWrite(ledv1, HIGH);// on fixe les sorties à 5V...
    digitalWrite(ledv2, HIGH);
    digitalWrite(ledj1, HIGH);
    digitalWrite(ledj2, HIGH);
    digitalWrite(ledr1, HIGH);
    digitalWrite(ledr2, HIGH);
    digitalWrite(ledvp1, HIGH);
    digitalWrite(ledvp2, HIGH);
    digitalWrite(ledrp1, HIGH);
    digitalWrite(ledrp2, HIGH);

    delay(100);
  }
  Serial.print("loop");// on écrit sur le port série "loop" (le port série c'est la connection USB, et on peut lire c'est commentaire avec le moniteur série
  delay(500);

}// pas oublier de fermer la boucle

void loop() {
}
